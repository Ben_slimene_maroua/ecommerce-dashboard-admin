import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { ProductService } from '../product.service';
import { ProviderService } from '../provider.service';
import { SubcategorieService } from '../subcategorie.service';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {
  AddProductForm: FormGroup;
  submitted = false;
listprovider:any;
listsubcat: any;
myFiles:string[]=[];


  constructor(private productservice: ProductService,private formbuilder:FormBuilder,private providerservice: ProviderService, private subcategorieservice:SubcategorieService) { }

  ngOnInit(): void {
    
    this.AddProductForm = this.formbuilder.group({
      nom: ['', Validators.required],
      description: ['', Validators.required],  
      prix: ['', Validators.required],
      quantite: ['', Validators.required],
      reference: ['', Validators.required],
      color: ['', Validators.required],
      provider:['',Validators.required],
      subcat:['',Validators.required],
      pictures:['',Validators.required]
     
    }, );

    this.getallProvider();
    this.getallsubcat();
  }
  onFileChange(event:any){
    for(var i=0; i< event.target.files.length; i++){
      this.myFiles.push(event.target.files[i]);
    }
  }
  getallProvider(){
    return this.providerservice.getallproviders().subscribe((res:any)=>{
    this.listprovider=res["data"]
    })
  }
  getallsubcat(){
    return this.subcategorieservice.getallsubcategories().subscribe((res:any)=>{
      this.listsubcat=res["data"]
    })
  }


  get f() { return this.AddProductForm.controls; }

  onSubmit() {
      this.submitted = true;

       let formdata= new FormData();
       formdata.append("nom",this.AddProductForm.value.nom);
       formdata.append("description",this.AddProductForm.value.description);
       formdata.append("prix",this.AddProductForm.value.prix);
       formdata.append("quantite",this.AddProductForm.value.quantite);
       formdata.append("reference",this.AddProductForm.value.reference);
       formdata.append("color",this.AddProductForm.value.color);
       formdata.append("provider",this.AddProductForm.value.provider);
       formdata.append("subcat",this.AddProductForm.value.subcat);

       for (var i=0; i< this.myFiles.length;i++){
       formdata.append("pictures",this.myFiles[i]);
      }

      this.productservice.addproduct(formdata).subscribe((res:any)=>{
          console.log("response",res) 
          Swal.fire("Produit ajouté")
      })
      
  }

  onReset() {
      this.submitted = false;
      this.AddProductForm.reset();
  }
}
