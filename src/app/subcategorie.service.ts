import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SubcategorieService {

  constructor(private http:HttpClient) { }
  getallsubcategories(){
    return this.http.get(`${environment.baseUrl}/subcategory/getSubcat`);
  }
  deleteSubcat(id:any){
    return this.http.delete(`${environment.baseUrl}/subcategory/deleteSubCat/${id}`);
  }
  updateSubcat(id:any,subcat:any){
    return this.http.put(`${environment.baseUrl}/subcategory/upSubcat/${id}`,subcat);
  }
  addsubcat(subcat:any){
    return this.http.post(`${environment.baseUrl}/subcategory/createSubcat`,subcat);
  }
}
