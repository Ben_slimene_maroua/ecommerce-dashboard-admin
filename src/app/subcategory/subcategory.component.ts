import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { SubcategorieService } from '../subcategorie.service';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.css']
})
export class SubcategoryComponent implements OnInit {
listsubcat: any;
updateForm: FormGroup;
search_nom: any;
p:number=1;
closeResult = '';  

  constructor(private subcategoryservice: SubcategorieService, private formbuilder: FormBuilder,private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getsubcategorie();
    this.updateForm = this.formbuilder.group({
      _id: ['', Validators.required],
      nom: ['', Validators.required],
      description: ['', Validators.required],
      //category:['', Validators.required]
    })
  }
getsubcategorie(){
  return this.subcategoryservice.getallsubcategories().subscribe((res:any)=>{
    this.listsubcat=res["data"];
  })
}

deletesubcat(id: any) {
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!',
  }).then((result) => {
    if (result.isConfirmed) {
      this.subcategoryservice.deleteSubcat(id).subscribe((res: any) => {
        Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
        this.getsubcategorie();
      });
    }
  });
}
upSubcat(){
  return this.subcategoryservice.updateSubcat(this.updateForm.value._id,this.updateForm.value).subscribe((res:any)=>{
    Swal.fire("Subcategorie modifié")
    this.getsubcategorie()
  })
}

open(content:any,listsubcat:any) {
  this.updateForm.patchValue({
    _id:listsubcat._id,
    nom:listsubcat.nom,
    description:listsubcat.description,
   
  })
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}

private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return `with: ${reason}`;
  }
}
}

