import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  constructor(private http:HttpClient) { }
  getallcategories(){
    return this.http.get(`${environment.baseUrl}/category/afficheCategory`);
  }
}
