import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http:HttpClient) { }

  getproducts(){
    return this.http.get(`${environment.baseUrl}/product/afficheProduct`);
  }; 
  deleteproduct(id:any){
    return this.http.delete(`${environment.baseUrl}/product/deleteProduct/${id}`);
  }
  updateproduct(id:any,product:any){
    return this.http.put(`${environment.baseUrl}/product/upProduct/${id}`,product);
  }
  addproduct(product:any){
    return this.http.post(`${environment.baseUrl}/product/createProduct`,product);
  }
}
