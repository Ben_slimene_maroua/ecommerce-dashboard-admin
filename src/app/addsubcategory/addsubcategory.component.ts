import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { CategorieService } from '../categorie.service';
import { SubcategorieService } from '../subcategorie.service';

@Component({
  selector: 'app-addsubcategory',
  templateUrl: './addsubcategory.component.html',
  styleUrls: ['./addsubcategory.component.css']
})
export class AddsubcategoryComponent implements OnInit {
addForm: FormGroup;
submitted = false;
listcategorie:any;
  constructor(private subcatservice:SubcategorieService,private formbuilder: FormBuilder, private categorieservice:CategorieService) { }

  ngOnInit(): void {
    this.addForm=this.formbuilder.group({
      nom:['',Validators.required],
      description:['',Validators.required],
      category:['',Validators.required]
    })
    this.getcategory();
  }
  get f() { return this.addForm.controls; }

  onSubmit() {
      this.submitted = true;
      this.subcatservice.addsubcat(this.addForm.value).subscribe((res:any)=>{
          console.log("response",res)
          Swal.fire("Subcategorie ajouté")
        
      })
      
  }

  onReset() {
      this.submitted = false;
      this.addForm.reset();
  }

  getcategory(){
    return this.categorieservice.getallcategories().subscribe((res:any)=>{
      this.listcategorie= res["data"];
    })
  }
}
