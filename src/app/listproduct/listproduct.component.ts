import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-listproduct',
  templateUrl: './listproduct.component.html',
  styleUrls: ['./listproduct.component.css'],
})
export class ListproductComponent implements OnInit {
  listproduct: any;
  p: number = 1; //pagination
  search_nom: any;
  closeResult = '';   
  updateForm: FormGroup;
  constructor(private productservice: ProductService, private formbuilder: FormBuilder,private modalService: NgbModal) {}

  ngOnInit(): void {
    this.updateForm = this.formbuilder.group({
      _id: ['', Validators.required],
      nom: ['', Validators.required],
      description: ['', Validators.required],
      prix:['',Validators.required],
      quantite:['', Validators.required],
      reference:['', Validators.required],
      // subcat:['', Validators.required],
      // provider:['', Validators.required]
    })

    this.getallproducts();
  }
  getallproducts() {
    this.productservice.getproducts().subscribe((res: any) => {
      this.listproduct = res['data'];
      console.log('list product', this.listproduct);
    });
  }
  deleteproduct(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.productservice.deleteproduct(id).subscribe((res: any) => {
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
          this.getallproducts();
        });
      }
    });
  }




  
  upProduct(){
    return this.productservice.updateproduct(this.updateForm.value._id,this.updateForm.value).subscribe((res:any)=>{
      Swal.fire("Produit modifié")
      this.getallproducts()
    })

  }

  open(content:any,listproduct:any) {
    this.updateForm.patchValue({
      _id:listproduct._id,
      nom:listproduct.nom,
      description:listproduct.description,
      prix:listproduct.prix,
      quantite:listproduct.quantite,
      reference: listproduct.reference,
 
    })
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
