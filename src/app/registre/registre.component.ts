import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ClientService } from '../client.service';
import { RegistreService } from '../registre.service';

@Component({
  selector: 'app-registre',
  templateUrl: './registre.component.html',
  styleUrls: ['./registre.component.css']
})
export class RegistreComponent implements OnInit {
 regForm: FormGroup;
 submitted=false;
 fileToUpload:Array<File>=[];  //implementer image
  constructor(private registreservice:RegistreService, private formbuilder: FormBuilder, private route:Router) { }

  ngOnInit(): void {
    this.regForm=this.formbuilder.group({
      nom:['',Validators.required],
      prenom:['',Validators.required],
      adresse:['',Validators.required],
      email:['',Validators.required],
      password:['',Validators.required],
    })
  }
  //implementer image
  handleFileInput(files:any){
    this.fileToUpload=<Array<File>>files.target.files;
    console.log(this.fileToUpload)
  }
  get f() { return this.regForm.controls; }

  onSubmit() {
      this.submitted = true;
       //implementer image: 
      let formdata= new FormData();
      formdata.append("nom",this.regForm.value.nom);
      formdata.append("prenom",this.regForm.value.prenom);
      formdata.append("adresse",this.regForm.value.adresse);
      formdata.append("email",this.regForm.value.email);
      formdata.append("password",this.regForm.value.password);
      formdata.append("image",this.fileToUpload[0]);


 // .addclient(this.regForm.value) -->addclient(formdata): implementer image
      this.registreservice.addclient(formdata).subscribe((res:any)=>{
          console.log("response",res)
          Swal.fire("Client ajouté")
      })
      this.route.navigateByUrl('/home/listclient')
  }
  onReset() {
      this.submitted = false;
      this.regForm.reset();
  }
}
