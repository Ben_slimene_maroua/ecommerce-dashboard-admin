import { TestBed } from '@angular/core/testing';

import { SubcategorieService } from './subcategorie.service';

describe('SubcategorieService', () => {
  let service: SubcategorieService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubcategorieService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
