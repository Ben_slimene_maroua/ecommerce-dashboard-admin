import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddproductComponent } from './addproduct/addproduct.component';
import { AddsubcategoryComponent } from './addsubcategory/addsubcategory.component';
import { ClientComponent } from './client/client.component';
import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { ListproductComponent } from './listproduct/listproduct.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { RegistreComponent } from './registre/registre.component';
import { SubcategoryComponent } from './subcategory/subcategory.component';

const routes: Routes = [
  {path:'',component:LoginComponent},
  {path:'registre',component:RegistreComponent},
  {
    path: 'home',canActivate:[AuthGuard],            //canActivate:[Authguard]: pour securiser les routes tant que nn connecté tu peux pas acceder a auccune interface
    component: HomeComponent,
    children: [
      { path: '', component: LayoutComponent },
      { path: 'addproduct', component: AddproductComponent },
      { path: 'listproduct', component: ListproductComponent },
      {path:'listclient',component:ClientComponent},
      {path:'addsubcat',component:AddsubcategoryComponent},
      {path:'listsubcat',component:SubcategoryComponent},
      {path:'profile',component:ProfileComponent}
      
   
    ],
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
